import json
import math
import os.path as osPath
import sys

import utils
from generator import makeRandVec, makeRandomWeights
from model import processData

inputFileAM = "learn_input_am.json"
inputFileNS = "learn_input_ns.json"


def am():
  inputData = utils.json_reader(inputFileAM)
  makeLearningProcessingFor(inputData["img_dir"], inputData["data_dir"], inputData["matrix_dir"])

def ns():
  inputData = utils.json_reader(inputFileNS)
  makeLearningProcessingFor(inputData["img_dir"], inputData["data_dir"], inputData["matrix_dir"])


def makeLearningProcessingFor(imgs_path, data_path, matrix_path):
  lettersData = {}
  for letter in utils.letters:
    lettersData[letter] = convertImgToBinary(imgs_path, matrix_path, letter)
    
  neuronsPerLv = getLevelsNeuronCounts(lettersData)
  makeOneLenForInputs(lettersData, neuronsPerLv[0])
  saveLettersData(data_path, utils.LEARNING_DATA_FILE, neuronsPerLv)

  rang_1 = (0, 1)
  rang_2 = (0, 1)
  for letter in utils.letters:
    currVector = lettersData[letter]['vector']
    levelsCount = len(neuronsPerLv)
    for i in range(1, levelsCount): # level on 0 need not pas and weights
      pasLen = neuronsPerLv[i]
      pas = makeRandVec(pasLen, rang_2) if i == levelsCount - 1 else makeRandVec(pasLen, rang_1)
      computeWeights(lettersData, currVector, pas, letter, neuronsPerLv[i], neuronsPerLv[i-1], i)
      currVector = pas
      saveLettersData(data_path, letter + '.json', lettersData[letter])
    
def convertImgToBinary(imgs_path, matrix_path, letter):
  binaryData = utils.processImage(imgs_path + letter + ".png")
  realData = utils.cutOnlyImageData(binaryData)
  # saveImgAsMatrix(matrix_path, letter, binaryData, realData)
  data = {}
  data['vector'] = utils.convertToVector(realData)
  data['width']  = len(realData)
  data['height'] = len(realData[0])
  return data

def saveImgAsMatrix(path, letter, binaryData, realData):
  img_matrix_path_big = osPath.join(path, letter + "_big.txt")
  utils.writeMatrixToFile(binaryData, img_matrix_path_big)

  img_matrix_path_small = osPath.join(path, letter + "_small.txt")
  utils.writeMatrixToFile(realData, img_matrix_path_small)

def getLevelsNeuronCounts(lettersData):
  inputCounts = []
  inputCounts.append(getMaxInputLen(lettersData))
  inputCounts.append(int(inputCounts[0] / 4))
  inputCounts.append(int(inputCounts[0] / 2))
  return inputCounts

def getMaxInputLen(lettersData):
  vectors = list(map(lambda data: data['vector'], lettersData.values()))
  return max(list(map(len, vectors)))

def makeOneLenForInputs(lettersData, correctLen):
  for letter in utils.letters:
    vector = lettersData[letter]['vector']
    lettersData[letter]['vector'] = utils.increaseVectorLen(correctLen, vector)

def computeWeights(lettersData, vector, pas, letter, width, height, levelNum):
  weights = makeRandomWeights(width, height)
  processData(vector, weights, pas)
  lettersData[letter]['weights' + str(levelNum)] = weights
  lettersData[letter]['pas' + str(levelNum)] = pas

def saveLettersData(path, filename, data):
  data_dir_am_path = osPath.join(path, filename)
  with open(data_dir_am_path, "w") as json_file:
    json.dump(data, json_file)
