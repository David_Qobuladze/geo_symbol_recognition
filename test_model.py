import unittest
import model
from halfLinear import HalfLinear
import generator

class TestModel(unittest.TestCase):

  def test_makeDotProduct(self):
    vec = [1, 0, 1]
    matrix = [
      [0, 0, 1],
      [1, 0, 1]
    ]
    dots = model._makeDotProduct(vec, matrix)
    self.assertSequenceEqual(dots, [1, 2])

  def est_makeDotProduct_bigData(self):
    length = 176
    x = generator.makeRandVec(length, (0, 1))
    print('x:', x)
    width = int(length / 2)
    w = generator.makeRandomWeights(width, length)
    dots = model._makeDotProduct(x, w)
    print ('dots', dots)
    self.assertTrue(True)

  def test_computeError(self):
    pas = [1, 0, 0, 1]
    out = [0, 0, 1, 1]
    diff = model._computeError(out, pas)
    self.assertSequenceEqual(diff, [-1, 0, 1, 0])

  def test_makeCorrectWeights(self):
    diff = [1, 0, -1]
    net = [4, 5, -3]
    x = [1, 0, 0, 1, 1]
    weights = [
      [-5,  2, 0,  2, 7],
      [ 0, -8, 1,  5, 0],
      [ 1,  3, 0, -4, 0]
    ]
    correctWeights = [
      [-7, 2, 0, 0, 5],
      [0, -8, 1, 5, 0],
      [3, 3, 0, -2, 2]
    ]
    result = model._makeCorrectWeights(diff, net, x, weights)
    for i in range(0, len(weights)):
      self.assertSequenceEqual(result[i], correctWeights[i])

  def test_processData(self):
    x = [1, 0, 0, 1, 1]
    w = [
      [-5,  2, 0,  2, 7],
      [ 0, -8, 1,  5, 0],
      [ 1,  3, 0, -4, 0]
    ]
    p = [0, 1, 1]
    resultW = model.processData(x, w, p)
    expectedW = [
      [-7, 2, 0, 0, 5],
      [0, -8, 1, 5, 0],
      [3, 3, 0, -2, 2]
    ]
    for i in range(0, len(w)):
      self.assertSequenceEqual(resultW[i], expectedW[i])

  def test_getRangesForErr(self):
    out = [-2, -1, 0, 1, 2]
    pas = [-3,  1, 0, 0, 4]
    expRanges = [((-70, -50), -1), ((10, 30), 1), ((None, None), None), ((-10, 10), -1), ((70, 90), 1)]
    err = model._computeError(out, pas)
    count = 4
    halfLinear = HalfLinear()
    halfLinear.generateRandges(count)
    result = model.getRangesForErr(err, pas, halfLinear)
    for i in range(0, len(out)):
      self.assertTupleEqual(result[i][0], expRanges[i][0])
      self.assertEqual(result[i][1], expRanges[i][1])


if __name__ == '__main__':
  unittest.main()