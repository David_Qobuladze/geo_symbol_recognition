import unittest
import generator

class TestGenerator(unittest.TestCase):

  def test_makeRandVec(self):
    size = 10
    randVec = generator.makeRandVec(size, (0, 1))
    self.assertEqual(size, len(randVec))

  def test_makeRandomWeights(self):
    w = 10
    h = 12
    matrix = generator.makeRandomWeights(w, h)
    self.assertTrue(w == len(matrix) and h == len(matrix[0]))

if __name__ == '__main__':
  unittest.main()