import math

class HalfLinear:

  def __init__(self):
    self._ranges = {}
    self._DIAP = 20
    self._startRange = (-10, 10)
    self._maxWeight = None

  def getRangFor(self, weight):
    ranges = list(filter(lambda rang: self._ranges[rang] == weight, self._ranges.keys()))
    return ranges[0]

  def getWeightFor(self, value):
    result = None
    intervals = list(filter(lambda rang: self._isInRange(rang, value), self._ranges.keys()))
    return self._ranges[intervals[0]]

  def generateRandges(self, count):
    # self._maxWeight = count
    self._ranges[self._startRange] = 0
    for i in range(1, count + 1):
      newRangeLeft  = (self._startRange[0] - i * self._DIAP, self._startRange[1] - i * self._DIAP)
      newRangeRight = (self._startRange[0] + i * self._DIAP, self._startRange[1] + i * self._DIAP)
      self._ranges[newRangeLeft] = 0 - i
      self._ranges[newRangeRight] = i
    leftOutRange  = (-math.inf, self._startRange[0] - count * self._DIAP)
    rightOutRange = (self._startRange[1] + count * self._DIAP,  math.inf)
    self._ranges[leftOutRange]  = 0 - (count + 1)
    self._ranges[rightOutRange] = count + 1
    return self._ranges

  def _isInRange(self, rang, value):
    # left closed interval - [min, max)
    return rang[0] <= value and value < rang[1] 