from random import randint


def makeRandomWeights(width, height, interval = (-10, 10)):
  weights = [[0 for j in range(0, height)] for i in range(0, width)]
  for x in range(0, width):
    for y in range(0, height):
      weights[x][y] = randint(interval[0], interval[1])
  return weights

def makeRandVec(length, rang):
  pas = []
  for i in range(0, length):
    pas.append(randint(rang[0], rang[1]))
  return pas
