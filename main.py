import sys
import learning
import recognition
import utils

if __name__ == "__main__":
  process_flag = sys.argv[1]
  alphabet = sys.argv[2]
  if process_flag == '-l':
    if alphabet == '-a': learning.am()
    elif alphabet == '-n': learning.ns()
    else: print("Incorrect alphabet flag (-a or -n)")
  elif process_flag == '-r':
    if   alphabet == '-a': recognition.main(utils.getFilePath("reco_input_am.json"), True)
    elif alphabet == '-n': recognition.main(utils.getFilePath("reco_input_ns.json"), False)
    else: print("Incorrect alphabet flag (-a or -n)")
  else:
    print ("Incorrect process flag (-l or -r)")
