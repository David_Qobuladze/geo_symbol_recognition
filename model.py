import utils

def processData(x, weights, pas):
  nets = _makeDotProduct(x, weights)
  # print('----------------------------')
  # print('nets', nets)
  # print('----------------------------')
  
  outs = utils.f(nets)
  # print('##############################')
  # print('outs:', outs)
  # print('##############################')

  diff = _computeError(outs, pas)
  # print('$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$')
  # print('diff:', diff)
  # print('$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$')

  return _makeCorrectWeights(diff, nets, x, weights)

def _makeDotProduct(x, weights):
  result = []
  for i in range(0, len(weights)):
    result.append(_multiply(x, weights[i]))
  return result

def _multiply(x, weight):
  result = 0
  for i in range(0, len(x)):
    result += x[i] * weight[i]
  return result

def _computeError(outs, pas):
  result = []
  for i in range(0, len(outs)):
    result.append(outs[i] - pas[i])
  return result

def _makeCorrectWeights(diff, nets, x, weights):
  for i in range(0, len(diff)):
    elem = diff[i]
    if elem == 0: continue
    coeff = _computeCoeff(nets[i], x)
    _correctWeights(weights[i], coeff, x, elem < 0)
  return weights

def _computeCoeff(net, x):
  return int(abs(net) / sum(x)) + 1

def _correctWeights(weight, coeff, x, isIncreased):
  for i in range(0, len(weight)):
    number = coeff * x[i]
    weight[i] += number if isIncreased else -number



def processDataHalfLinear(x, weights, pas, halfLinear):
  nets = _makeDotProduct(x, weights)
  outs = [halfLinear.getWeightFor(n) for n in nets]
  errs = _computeError(outs, pas)
  rangesWithFlag = getRangesForErr(errs, pas, halfLinear)
  for i in range(0, len(rangesWithFlag)):
    rf = rangesWithFlag[i]
    if rf[1] == None: continue
    

def getRangesForErr(err, pas, halfLinear):
  result = []
  for i in range(0, len(err)):
    e = err[i]
    if e == 0:
      result.append(((None, None), None))
    else:
      rang = halfLinear.getRangFor(pas[i])
      result.append((rang, -1 if e > 0 else 1))
  return result


def rameMethod(x, weights, rangesWithFlag):
  for i in range(0, len(rangesWithFlag)):
    rf = rangesWithFlag[i]
    if rf[1] == None: continue