import sys
import unittest
import utils 

class TestUtils(unittest.TestCase):

  def test_convertToVector(self):
    matrix = [
      [1, 1, 0],
      [0, 1, 1]
    ]
    vec = utils.convertToVector(matrix)
    self.assertSequenceEqual(vec, [1, 0, 1, 1, 0, 1])

  def test_increaseVectorLen(self):
    vec = [1, 1]
    correctLen = 5
    newVec = utils.increaseVectorLen(correctLen, vec)
    self.assertEqual(len(newVec), correctLen)
    self.assertSequenceEqual(newVec, [1, 1, 0, 0, 0])

  def test_f(self):
    net = [10, -10, 0]
    out = utils.f(net)
    self.assertSequenceEqual(out, [1, 0, 1])

if __name__ == '__main__':
  unittest.main()