import json
import math
import os.path as osPath
from PIL import Image

IMG_WHITE_VAL =  0
IMG_BLACK_VAL =  1

letters = [
    "1_0", "1_1", "1_2", "1_3", "1_4", "1_5", "1_6", "1_7", "1_8", "1_9", "1_10",
    "2_0", "2_1", "2_2", "2_3", "2_4", "2_5", "2_6", "2_7", "2_8", "2_9", "2_10",
    "3_0", "3_1", "3_2", "3_3", "3_4", "3_5", "3_6", "3_7", "3_8", "3_9", "3_10",
    "4_0", "4_1", "4_2", "4_3", "4_4", "4_5", "4_6", "4_7", "4_8", "4_9", "4_10",
    "5_0", "5_1", "5_2", "5_3", "5_4", "5_5", "5_6", "5_7", "5_8", "5_9", "5_10",
    "6_0", "6_1", "6_2", "6_3", "6_4", "6_5", "6_6", "6_7", "6_8", "6_9", "6_10",
    "7_0", "7_1", "7_2", "7_3", "7_4", "7_5", "7_6", "7_7", "7_8", "7_9", "7_10",
    "8_0", "8_1", "8_2", "8_3", "8_4", "8_5", "8_6", "8_7", "8_8", "8_9", "8_10",
    "9_0", "9_1", "9_2", "9_3", "9_4", "9_5", "9_6", "9_7", "9_8", "9_9", "9_10",
    "10_0", "10_1", "10_2", "10_3", "10_4", "10_5", "10_6", "10_7", "10_8", "10_9", "10_10",
    "11_0", "11_1", "11_2", "11_3", "11_4", "11_5", "11_6",
    "12_0", "12_1", "12_2", "12_3", "12_4", "12_5", "12_6",
    "13_0", "13_1", "13_2", "13_3", "13_4", "13_5", "13_6",
    "14_0", "14_1", "14_2", "14_3", "14_4", "14_5", "14_6",
    "15_0", "15_1", "15_2", "15_3", "15_4", "15_5", "15_6",
    "16_0", "16_1", "16_2", "16_3", "16_4", "16_5", "16_6",
    "17_0", "17_1", "17_2", "17_3", "17_4", "17_5", "17_6",
    "18_0", "18_1", "18_2", "18_3", "18_4", "18_5", "18_6",
    "19_0", "19_1", "19_2", "19_3", "19_4", "19_5", "19_6",
    "20_0", "20_1", "20_2", "20_3", "20_4", "20_5", "20_6",
    "21_0", "21_1", "21_2", "21_3", "21_4", "21_5", "21_6",
    "22_0", "22_1", "22_2", "22_3", "22_4", "22_5", "22_6",
    "23_0", "23_1", "23_2", "23_3", "23_4", "23_5", "23_6",
    "24_0", "24_1", "24_2", "24_3", "24_4", "24_5", "24_6",
    "25_0", "25_1", "25_2", "25_3", "25_4", "25_5", "25_6",
    "26_0", "26_1", "26_2", "26_3", "26_4", "26_5", "26_6",
    "27_0", "27_1", "27_2", "27_3", "27_4", "27_5", "27_6",
    "28_0", "28_1", "28_2", "28_3", "28_4", "28_5", "28_6",
    "29_0", "29_1", "29_2", "29_3", "29_4", "29_5", "29_6",
    "30_0", "30_1", "30_2", "30_3", "30_4", "30_5", "30_6",
    "31_0", "31_1", "31_2", "31_3", "31_4", "31_5", "31_6",
    "32_0", "32_1", "32_2", "32_3", "32_4", "32_5", "32_6",
    "33_0", "33_1", "33_2", "33_3", "33_4", "33_5", "33_6",
    "34_0", "34_1", "34_2", "34_3", "34_4", "34_5", "34_6",
    "35_0", "35_1", "35_2", "35_3", "35_4", "35_5", "35_6",
    "36_0", "36_1", "36_2", "36_3", "36_4", "36_5", "36_6",
    "37_0", "37_1", "37_2", "37_3", "37_4", "37_5", "37_6",
    "38_0", "38_1", "38_2", "38_3", "38_4", "38_5", "38_6",
    ]

WHITE = (255, 255, 255, 255)
WHITE_DIFF = (5, 5, 5, 255)

LEARNING_DATA_FILE = "learning_data.json"

def processImage(path):
  img = Image.open(path, "r")
  rgbaArray = list(img.getdata())
  imgWidth  = img.size[0]
  imgHeight = img.size[1]
  counter = 0
  height = 0
  matrix = [[0 for h in range(imgHeight)] for w in range(imgWidth)]

  for rgba in rgbaArray:
    if counter == imgWidth:
      counter = 0
      height += 1
    matrix[counter][height] = IMG_WHITE_VAL if _isPixelWhite(rgba) else IMG_BLACK_VAL
    counter += 1
  return matrix

def getFilePath(fileName):
  cwd = osPath.dirname(__file__)
  return osPath.join(cwd, fileName)

def inputReader(inputFileName):
  inputFile = open(getFilePath(inputFileName), "r")
  alphabet = inputFile.readline().strip().split(",")
  inputFile.readline() # read empty line
  img_dir_am = inputFile.readline().strip()
  matrix_dir_am = inputFile.readline().strip()
  data_dir_am = inputFile.readline().strip()
  inputFile.close()
  return (alphabet, img_dir_am, matrix_dir_am, data_dir_am)

def json_reader(fileName, containsPath = False):
  path = fileName if containsPath else getFilePath(fileName)
  with open(path, "r") as json_file:
    return json.load(json_file)


def _isPixelWhite(pixel):
  for i in range(0, len(pixel)):
    # if WHITE[i] - WHITE_DIFF[i] > pixel[i]:
    if pixel[i] != WHITE[i]:
      return False
  return True


def _findBorders(matrix):
  x_s = math.inf
  y_s = math.inf
  x_e = 0
  y_e = 0
  for y in range(0, len(matrix[0])):
    for x in range(0, len(matrix)):
      if matrix[x][y] == 1:
        if x < x_s:
          x_s = x
        if x > x_e:
          x_e = x
        if y < y_s:
          y_s = y
        if y > y_e:
          y_e = y
  return (y_s, x_e, y_e, x_s)

def cutOnlyImageData(matrix):
  top, right, bottom, left = _findBorders(matrix)
  newMatrix = [[0 for h in range(bottom - top + 1)] for w in range(right - left + 1)]
  for b in range(top, bottom + 1):
    for a in range(left, right + 1):
      newMatrix[a-left][b-top] =  matrix[a][b]
  return newMatrix

def writeMatrixToFile(matrix, filePath):
  file = open(filePath, "w")
  for y in range(0, len(matrix[0])):
    for x in range(0, len(matrix)):
      file.write(str(matrix[x][y]))
    file.write("\n")
  file.close()

def convertToVector(matrix):
  result = []
  for j in range(0, len(matrix[0])):
    for i in range(0, len(matrix)):
      result.append(matrix[i][j])
  return result

def makeDotProduct(x, weights):
  result = []
  for i in range(0, len(weights)):
    result.append(_multiply(x, weights[i]))
  return result

def _multiply(x, weight):
  result = 0
  for i in range(0, len(x)):
    result += x[i] * weight[i]
  return result

def f(nets):
  result = []
  for elem in nets:
    result.append(1 if elem >= 0 else 0)
  return result

def f_hl(halfLinear, nets):
  result = []
  for elem in nets:
    result.append(halfLinear.getWeightFor(elem))
  return result

def cut_letter_from(path):
  return path[path.rfind("/")+1:path.rfind(".")]

def increaseVectorLen(correctLen, vector):
  modifedInputVec = [IMG_WHITE_VAL for i in range(0, correctLen)]
  for i in range(0, len(vector)):
    modifedInputVec[i] = vector[i]
  return modifedInputVec