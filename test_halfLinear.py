import unittest
from halfLinear import HalfLinear
import math

class TestHalfLiear(unittest.TestCase):

  RANGES = {
      (-math.inf, -90): -5,
      (-90, -70): -4,
      (-70, -50): -3,
      (-50, -30): -2,
      (-30, -10): -1,
      (-10, 10): 0,
      (10, 30): 1,
      (30, 50): 2,
      (50, 70): 3,
      (70, 90): 4,
      (90, math.inf): 5
    }

  def test_generateRanges(self):
    halfLinear = HalfLinear()
    count = 4
    resRanges = halfLinear.generateRandges(count)
    self.assertDictEqual(resRanges, self.RANGES)
  
  def test_getWeightFor(self):
    halfLinear = HalfLinear()
    count = 4
    ranges = halfLinear.generateRandges(count)
    values =  [-100,   -89, -69, -49, -29, -9, 11, 31, 51, 71,   100]
    weights = [  -5,    -4,  -3,  -2,  -1,  0,  1,  2,  3,  4,     5]
    for i in range(0, len(values)):
      value = values[i]
      resWeight = halfLinear.getWeightFor(value)
      self.assertEqual(resWeight, weights[i])

  def test_getRangFor(self):
    halfLinear = HalfLinear()
    count = 4
    ranges = halfLinear.generateRandges(count)
    weights = [-5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5]
    ranges  = list(self.RANGES.keys())
    for i in range(0, len(weights)):
      self.assertTupleEqual(halfLinear.getRangFor(weights[i]), ranges[i])

if __name__ == '__main__':
  unittest.main()

