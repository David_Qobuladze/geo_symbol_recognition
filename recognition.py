import utils


def countSimilarityPercent(out, pas):
  succCoef = 1
  errCoeff = 1.25
  same = 0
  for i in range(0, len(out)):
    if out[i] == pas[i]:
      same += succCoef
    else:
      same -= 0 if same <= errCoeff else errCoeff
  return round(same / len(pas), 2)

def takeSecond(elem):
  return elem[1]

def main(inputFileName, isAm):
  inputData = utils.json_reader(inputFileName)
  result = []
  binaryData = utils.processImage(inputData["img_path"])
  realData = utils.cutOnlyImageData(binaryData)

  # input_letter = utils.cut_letter_from(inputData["img_path"])
  # utils.writeMatrixToFile(binaryData, inputData["matrix_path"] + input_letter + "_.txt")
  # utils.writeMatrixToFile(realData, inputData["matrix_path"] + input_letter + ".txt")

  learningData = utils.json_reader(inputData["data_path"] + utils.LEARNING_DATA_FILE, True)

  levelNum = len(learningData) - 1
  inputLength = learningData[0]
  vector = utils.convertToVector(realData)

  if len(vector) > inputLength:
    print('Refuse!')
  else:
    vector_x = utils.increaseVectorLen(inputLength, vector)
    for letter in utils.letters:
      data = utils.json_reader(inputData["data_path"] + letter + ".json", True)
      out = []
      x = vector_x
      for i in range(0, levelNum):
        nets = utils.makeDotProduct(x, data["weights" + str(i + 1)])
        out = utils.f(nets)
        x = out
      result.append((letter, countSimilarityPercent(out, data["pas2"])))
    result.sort(reverse=True, key=takeSecond)
    print(result)
