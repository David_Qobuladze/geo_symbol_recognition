# Geo_Symbol_Recognition

### პროგრამის გაშვება ხდება შემდეგნაირად:

**სწავლების პროცესისათვის:**

```shell
python3 main.py -l learn_input.json -d
```

სადაც learn_input.json აქვს შემდეგი სტრუქტურა:

```json
{
    "img_dir_am": "path/to/images/am/",
    "img_dir_ns": "path/to/images/ns/",
    "data_dir_am": "path/to/data/asomtavruli/",
    "data_dir_ns": "path/to/data/nusxuri/"
}
```


**ამოცნობის პროცესისათვის:**

```shell
python3 main.py -r reco_input.json
```

სადაც reco_input.json აქვს შემდეგი სტრუქტურა:

```json
{
  "img_path": "path/to/image.png",
  "data_path_am": "path/to/data/am/",
  "data_path_ns": "path/to/data/ns/"
}

```